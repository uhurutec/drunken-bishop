/* drunken_bishop.c
 *
 * Copyright 2022 Max Harmathy <maxh@if.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drunken_bishop.h"
#include <string.h>

// We divide each byte into four bit pairs.
#define BIT_PAIRS_PER_CHAR 4

// The selector value 0xC0 (in binary 11000000) selects the first two bits.
#define SELECTOR 0xC0

// To get simple numbers from 0 to 3, we have to shift for six bits
#define SHIFTS 6

// The data itself has to be shifted by two bits after each step
#define DATA_SHIFT 2

void drubis_init_parameters(drubis_parameters* parameters) {
    parameters->input.data = NULL;
    parameters->input.length = 0;
    parameters->width = DRUBIS_DEFAULT_WIDTH;
    parameters->height = DRUBIS_DEFAULT_HEIGHT;
}

void drubis_init_play_field(
    drubis_play_field* play_field, unsigned int width, unsigned int height
) {
    size_t play_field_size = sizeof(unsigned int) * width * height;
    play_field->width = width;
    play_field->height = height;
    play_field->squares = malloc(play_field_size);
    memset(play_field->squares, 0, play_field_size);
}

void drubis_destroy_play_field(drubis_play_field* play_field) {
    free(play_field->squares);
    play_field->squares = NULL;
}

struct position {
    unsigned int x, y;
};

unsigned int min(unsigned int a, unsigned int b) {
    if (a < b) {
        return a;
    }
    return b;
}

#ifdef DRUNKEN_BISHOP_DEBUG_OUTPUT
// output all the steps on debug builds
#define DEBUG_OUTPUT_PLAY_FIELD_STEPS                                                  \
    fprintf(                                                                           \
        stderr, "data: [0x%02X: %c%c%c%c%c%c%c%c] point: {%c%c} dir: %s (%ud, %ud)\n", \
        data, (data & 0x80 ? '1' : '0'), (data & 0x40 ? '1' : '0'),                    \
        (data & 0x20 ? '1' : '0'), (data & 0x10 ? '1' : '0'),                          \
        (data & 0x08 ? '1' : '0'), (data & 0x04 ? '1' : '0'),                          \
        (data & 0x02 ? '1' : '0'), (data & 0x01 ? '1' : '0'),                          \
        (data_point & 0x02 ? '1' : '0'), (data_point & 0x01 ? '1' : '0'),              \
        direction_symbol, current_position.x, current_position.y                       \
    );
#else
#define DEBUG_OUTPUT_PLAY_FIELD_STEPS
#endif

drubis_play_field drubis_let_him_go(drubis_parameters parameters) {
    drubis_play_field playField;
    drubis_init_play_field(&playField, parameters.width, parameters.height);

    struct position current_position = {
        .x = parameters.width / 2, .y = parameters.height / 2};

    u_int8_t* input = parameters.input.data;

    for (unsigned int index = 0; index < parameters.input.length; ++index) {
        u_int8_t data = input[index];
        for (unsigned int pair = 0; pair < BIT_PAIRS_PER_CHAR; ++pair) {
            char* direction_symbol;
            int data_point = (data & SELECTOR) >> SHIFTS;

            switch (data_point) {
            case 0: {
                if (current_position.x != 0) {
                    --(current_position.x);
                }
                if (current_position.y != parameters.height - 1) {
                    ++(current_position.y);
                }
                direction_symbol = "↖";
                break;
            }
            case 1: {
                if (current_position.x != parameters.width - 1) {
                    ++(current_position.x);
                }
                if (current_position.y != parameters.height - 1) {
                    ++(current_position.y);
                }
                direction_symbol = "↗";
                break;
            }
            case 2: {
                if (current_position.x != 0) {
                    --(current_position.x);
                }
                if (current_position.y != 0) {
                    --(current_position.y);
                }
                direction_symbol = "↙";
                break;
            }
            case 3: {
                if (current_position.x != parameters.width - 1) {
                    ++(current_position.x);
                }
                if (current_position.y != 0) {
                    --(current_position.y);
                }
                direction_symbol = "↘";
                break;
            }
            default:
                fprintf(stderr, "data point %d not implemented\n", data_point);
                continue;
            }
            DEBUG_OUTPUT_PLAY_FIELD_STEPS
            data <<= DATA_SHIFT;
            unsigned int field_index =
                current_position.y * playField.width + current_position.x;
            ++(playField.squares[field_index]);
        }
    }
    return playField;
}

void draw_horizontal_border(char* target, unsigned int width) {
    target[0] = BORDER_CORNER;
    for (int i = 1; i <= width; ++i) {
        target[i] = BORDER_HORIZONTAL;
    }
    target[width + 1] = BORDER_CORNER;
    target[width + 2] = '\n';
}

char* drubis_play_field_to_ascii_art(drubis_play_field field) {

    // border adds two additional chars plus one line break per row
    unsigned int row_length = field.width + 3;

    // border adds two additional rows
    unsigned int result_length = row_length * (field.height + 2);

    char* result = malloc(sizeof(char) * (result_length + 1));
    result[result_length] = '\0';

    draw_horizontal_border(result, field.width);
    char* result_position = result + row_length;
    for (int row = 0; row < field.height - 1; ++row) {

        *result_position = BORDER_VERTICAL;
        ++result_position;

        unsigned int current_row = (field.height - row - 1) * field.width;
        for (int column = 0; column < field.width; ++column) {
            *result_position =
                drubis_figures[min(field.squares[current_row + column], 14)];
            ++result_position;
        }

        result_position[0] = BORDER_VERTICAL;
        result_position[1] = '\n';
        result_position += 2;
    }
    draw_horizontal_border(result_position, field.width);

    return result;
}
