#!/bin/bash
# style check using clang-format
#
# Copyright 2022 Max Harmathy <maxh@if.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e
shopt -s nullglob
shopt -s globstar

temp_dir=$(mktemp -d)

test_count='0'
failed_count='0'

for file in **/*.{h,c}
do
  path="$(dirname "$file")"
  mkdir -p "$temp_dir/$path"
  clang-format "$file" > "$temp_dir/$file"
  ((++test_count))
  if ! udiff=$(diff "$file" "$temp_dir/$file")
  then
    echo "[FAILED] $file"
    echo "$udiff" >&2
    ((++failed_count))
  fi
done

rm -rf "$temp_dir"

if (( failed_count == 0 ))
then
  echo "all $test_count files passed"
else
  echo "$failed_count out of $test_count files failed"
  exit 1
fi

