This is a simple implementation of the [Drunken Bishop][1] algorithm. The
purpose is solely for playing around with the code and Gitlab CI.

Build
=====

For argument parsing [`agrp.h` from GNU C Library][2] is required. Code-wise
there are no other dependencies. For the build process CMake is used.

To build using Ninja build, run:

```sh
cmake -B build -G Ninja .
ninja -C build
```

Usage
=====

The program expects an input string as argument. The default behavior is to
simply interpret the string as a sequence of bytes and feed it into the Drunken
Bishop algorithm.

```sh
drunken_bishop 'Hello World!'
```

Output is the resulting ascii art image.

```
+-----------------+
|       .o*.o     |
|      .o* + .    |
|      o..= . *   |
|       .... * .  |
|             .   |
|                 |
|                 |
|                 |
+-----------------+
```

Using the parameter `-x` or `--hex` causes the program to interpret the input
string binary data in hex format.


---

[1]: http://www.dirk-loss.de/sshvis/drunken_bishop.pdf
[2]: https://www.gnu.org/software/libc/manual/html_node/Argp.html
